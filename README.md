# minigem

A project for learning Dart/Flutter while creating a Gemini client for Android.

## Links

### Gemini protocol

- [Spec](https://gemini.circumlunar.space/docs/specification.html)
- Markup intro: gemini://gemini.circumlunar.space/docs/gemtext.gmi

### Dart

- [Language tour](https://dart.dev/guides/language/language-tour)
- [Core libraries](https://dart.dev/guides/libraries/library-tour)
- [TLS](https://api.dart.dev/stable/2.8.4/dart-io/SecureSocket-class.html)

### Flutter

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
- [online documentation](https://flutter.dev/docs)

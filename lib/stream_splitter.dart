import 'dart:async';

class IntSplitter extends StreamTransformerBase<int, List<int>> {
  final List<int> _separators;
  IntSplitter(this._separators);

  @override
  Stream<List<int>> bind(Stream<int> stream) {
    return Stream.eventTransformed(
        stream, (sink) => _SplitterSink(sink, _separators));
  }
}

class _SplitterSink implements EventSink<int> {
  final EventSink<List<int>> _outSink;
  List<int> _buf = <int>[];
  final List<int> _separators;
  _SplitterSink(this._outSink, this._separators);

  @override
  void add(int ev) {
    if (_separators.contains(ev)) {
      if (_buf.length > 0) {
        _outSink.add(_buf);
        _buf = <int>[];
      }
    } else {
      _buf.add(ev);
    }
  }

  @override
  void addError(Object error, [StackTrace stackTrace]) {
    _outSink.addError(error, stackTrace);
  }

  @override
  void close() {
    if (_buf.length > 0) {
      _outSink.add(_buf);
      _buf = <int>[];
    }
    _outSink.close();
  }
}

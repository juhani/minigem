import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:minigem/gemini_client.dart';

class SqliteCertStore implements CertStore {
  final Database db;
  SqliteCertStore({this.db});

  static Future<SqliteCertStore> open() async {
    final db =
        await openDatabase(join(await getDatabasesPath(), 'minigem_certs.db'),
            onCreate: (db, version) {
      return db.execute('CREATE TABLE certs(host TEXT PRIMARY KEY, sha1 TEXT)');
    }, version: 1);
    return SqliteCertStore(db: db);
  }

  Future<void> add(String certHash, String host, int port) async {
    await db.insert('certs', {'host': '$host:$port', 'sha1': certHash},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<bool> check(String certHash, String host, int port) async {
    List<Map> list = await db
        .rawQuery('SELECT sha1 FROM certs WHERE host = ?', ['$host:$port']);
    if (list.length < 1) {
      return false;
    }
    return list[0]['sha1'] == certHash;
  }
}

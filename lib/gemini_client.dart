import 'dart:convert';
import 'dart:io';

List<int> headerDelimiter = utf8.encode('\r\n');

class GeminiResponse {
  int status;
  String mimeType;
  String inputPrompt;
  String errorInfo;
  String redirectLocation;
  bool authorizationRequired = false;
  bool knownCert = false;
  X509Certificate cert;

  Stream<int> body;

  // parses response and returns response status
  Future<int> parse(Stream<int> s) async {
    final headerBytes =
        await s.takeWhile((element) => element != headerDelimiter[0]).toList();
    final headerDelimCheck = await s.take(1).toList();
    if (headerDelimiter[1] != headerDelimCheck[0]) {
      throw 'invalid response (header delimiter)';
    }
    if (headerBytes.length < 3) {
      throw 'invalid response (header too short)';
    }
    if (headerBytes.length > 1024 + 3) {
      throw 'invalid response (header too long)';
    }

    status = int.tryParse(String.fromCharCodes(headerBytes, 0, 2));
    final meta = String.fromCharCodes(headerBytes, 3);

    if (status == null) {
      throw 'invalid response (statuscode is not a number)';
    }

    if (status >= 10 && status < 20) {
      // INPUT required
      inputPrompt = meta;
    } else if (status >= 20 && status < 30) {
      // SUCCESS
      if (meta != 'text/gemini') {
        throw 'mime type not supported ($meta)';
      }
      mimeType = meta;
      body = s;
    } else if (status >= 30 && status < 40) {
      // REDIRECT
      redirectLocation = meta;
    } else if (status >= 40 && status < 60) {
      // FAILURE
      errorInfo = meta;
    } else if (status >= 60 && status < 70) {
      // AUTHORIZATION REQUIRED
      authorizationRequired = true;
    } else {
      throw 'invalid status code';
    }
    return status;
  }
}

abstract class CertStore {
  Future<void> add(String certHash, String host, int port);
  Future<bool> check(String certHash, String host, int port);
}

class GeminiClient {
  CertStore certStore;

  GeminiClient(CertStore certStore) {
    this.certStore = certStore;
  }

  Future<GeminiResponse> get(Uri uri) async {
    bool validateCert(X509Certificate cert) {
      final now = DateTime.now();
      if (cert.endValidity.isBefore(now) || cert.startValidity.isAfter(now)) {
        return false;
      }
      if (!cert.subject.contains('/CN=${uri.host}')) {
        return false;
      }
      return true;
    }

    final port = uri.hasPort ? uri.port : 1965;
    final socket = await SecureSocket.connect(uri.host, port,
        onBadCertificate: validateCert, timeout: Duration(seconds: 7));
    final resp = GeminiResponse();
    resp.cert = socket.peerCertificate;
    resp.knownCert =
        await certStore.check(base64Encode(resp.cert.sha1), uri.host, port);
    socket.encoding = Encoding.getByName('UTF-8');
    socket.write(uri.toString() + '\r\n');
    socket.done.then((value) {
      print('DONE');
    });
    final byteStream = socket
        .expand((element) => element)
        .timeout(Duration(seconds: 15), onTimeout: (s) {
      socket.destroy();
    }).asBroadcastStream();
    await resp.parse(byteStream);
    return resp;
  }
}

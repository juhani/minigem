import 'dart:convert';
import 'package:flutter_test/flutter_test.dart';

import 'package:minigem/gemini_client.dart';

Stream<int> successResp = Stream.fromIterable(utf8.encode('20 text/gemini\r\n# title')).asBroadcastStream();
Stream<int> redirectResp = Stream.fromIterable(utf8.encode('31 gemini://foo.bar/\r\n')).asBroadcastStream();

class TrueCertStore implements CertStore {
  Future<bool> check(String certHash, String host, int port) async {
    return true;
  }

  Future<void> add(String certHash, String host, int port) async {
    return;
  }
}

class FalseCertStore implements CertStore {
  Future<bool> check(String certHash, String host, int port) async {
    return false;
  }

  Future<void> add(String certHash, String host, int port) async {
    return;
  }
}

void main() {
  test('parse success response', () async {
    final r = GeminiResponse();
    final status = await r.parse(successResp);
    expect(status, 20);
    expect(r.mimeType, 'text/gemini');
    expect(String.fromCharCodes(await r.body.toList()), '# title');
    expect(r.authorizationRequired, false);
  });
  test('parse redirect response', () async {
    final r = GeminiResponse();
    final status = await r.parse(redirectResp);
    expect(status, 31);
    expect(r.mimeType, isNull);
    expect(r.body, isNull);
    expect(r.redirectLocation, 'gemini://foo.bar/');
    expect(r.authorizationRequired, false);
  });
  test('successful request', () async {
    GeminiClient cli = GeminiClient(TrueCertStore());
    final r = await cli.get(Uri.parse('gemini://localhost/'));
    expect(r.status, 20);
    expect(r.knownCert, true);
  });
  test('redirect request', () async {
    GeminiClient cli = GeminiClient(FalseCertStore());
    final r = await cli.get(Uri.parse('gemini://localhost'));
    expect(r.status, 31);
    expect(r.redirectLocation, 'gemini://localhost/');
    expect(r.body, null);
    expect(r.knownCert, false);
  });
}

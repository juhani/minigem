import 'package:flutter_test/flutter_test.dart';

import 'package:minigem/cert_store.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  test('open store', () async {
    final store = await SqliteCertStore.open();
    expect(store, isNotNull);
  });

  test('store and check', () async {
    final store = await SqliteCertStore.open();
    expect(store, isNotNull);
    await store.add('dummyHash1', 'host', 1);
    final checkResult = await store.check('dummyHash1', 'host', 1);
    expect(checkResult, true);
  });

  test('changed sha', () async {
    final store = await SqliteCertStore.open();
    expect(store, isNotNull);
    await store.add('dummyHash1', 'host', 1);
    final checkResult = await store.check('dummyHash2', 'host', 1);
    expect(checkResult, false);
  });

  test('missing host', () async {
    final store = await SqliteCertStore.open();
    expect(store, isNotNull);
    final checkResult = await store.check('dummyHash2', 'host2', 1);
    expect(checkResult, false);
  });
}

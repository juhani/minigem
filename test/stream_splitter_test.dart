import 'dart:convert';
import 'package:flutter_test/flutter_test.dart';

import 'package:minigem/stream_splitter.dart';

Stream<int> s1 = Stream.fromIterable(utf8.encode('a\nb\n')).asBroadcastStream();
Stream<int> s2 =
    Stream.fromIterable(utf8.encode('alpha\nbeta\ngamma')).asBroadcastStream();
Stream<int> s3 = Stream.fromIterable(utf8.encode('alpha\nbeta\r\ngamma'))
    .asBroadcastStream();
List<int> separators = utf8.encode('\n\r');

void main() {
  test('s1', () async {
    final r = await s1
        .transform<List<int>>(IntSplitter(separators))
        .transform<String>(Utf8Decoder())
        .toList();
    expect(r[0], 'a');
    expect(r[1], 'b');
  });
  test('s2', () async {
    final r = await s2
        .transform<List<int>>(IntSplitter(separators))
        .transform<String>(Utf8Decoder())
        .toList();
    expect(r[0], 'alpha');
    expect(r[1], 'beta');
    expect(r[2], 'gamma');
  });
  test('s3', () async {
    final r = await s3
        .transform<List<int>>(IntSplitter(separators))
        .transform<String>(Utf8Decoder())
        .toList();
    expect(r[0], 'alpha');
    expect(r[1], 'beta');
  });
}
